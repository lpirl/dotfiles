This is the place where I keep configuration files
(which are not part of any of my Ansible roles, see below).

The configuration files in this repository and in
`my personal Ansible roles
<https://gitlab.com/lpirl/ansible-roles-personal>`__
are rather
individual and made public for reference, whereas the configuration
files in
`my generic Ansible roles
<https://gitlab.com/lpirl/ansible-roles-generic>`__,
`my specific Ansible roles
<https://gitlab.com/lpirl/ansible-roles-specific>`__
or
`my deprecated Ansible roles
<https://github.com/lpirl/ansible-roles>`__
are more likely to be reusable right away.

Generated files can be found `here <https://gitlab.com/lpirl/dotfiles/-/jobs/artifacts/master/browse/generated?job=generate_files>`__.

(Currently those files are still in
`another repository <https://github.com/lpirl/admintools>`__,
but it's planned to move them to this repository over time, slowly.)
