#!/usr/bin/env python3

"""
This script generates a shell wrapper to use the GNU time command for
generation of OpenMetrics. Useful, e.g, to record metrics in CI scripts.

The properties of the metrics and the format specifiers for GNU time are
taken from a CSV file with the script's name but 'csv' suffix.
"""

import sys
from os import chdir
from os.path import dirname, basename, splitext
from csv import reader as csv_reader
from shlex import quote

SUFFIXES = { # incomplete but sufficient for now
  'counter': 'total',
  'info': 'info',
}

def main():
  script_path = sys.argv[0]
  chdir(dirname(script_path) or '.')
  csv_path = splitext(basename(script_path))[0] + '.csv'
  fstring_parts = []
  with open(csv_path, newline='') as csv_file:
    skipped_header = False
    for row in csv_reader(csv_file, delimiter=';', quotechar='"'):

      if not row:
        continue

      if not skipped_header:
        skipped_header = True
        continue

      name, typ, unit, specifier, description = row

      suffix = SUFFIXES.get(typ, '')
      if suffix and not name.endswith(suffix):
        name = f'{name}_{suffix}'

      fstring_parts.append(f'# HELP {name} {description}')
      fstring_parts.append(f'# TYPE {name} {typ}')
      if unit:
        fstring_parts.append(f'# UNIT {name} {unit}')
      fstring_parts.append(f'{name} %{specifier}')

  fstring = quote('\\n'.join(fstring_parts))

  print(f'/usr/bin/env time -f {fstring} "$@"')

if __name__ == '__main__':
  main()
